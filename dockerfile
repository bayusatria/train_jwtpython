FROM python:3.7

#add code
ADD . /run
#set working directory
WORKDIR /run
#copy code
COPY . /run

#upgrade pip
RUN python -m pip install --upgrade pip
#install mssql package
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/ubuntu/16.04/prod.list > /etc/apt/sources.list.d/mssql-release.list
# install SQL Server drivers
RUN apt-get update && ACCEPT_EULA=Y apt-get install -y msodbcsql17 unixodbc-dev
# install SQL Server tools
RUN apt-get update && ACCEPT_EULA=Y apt-get install -y mssql-tools
RUN echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc
RUN /bin/bash -c "source ~/.bashrc"
#install package
RUN pip install -r requirements.txt
#set environtment variable
ENV FLASK_APP=run.py
#run the application
ENTRYPOINT ["flask", "run", "--host", "0.0.0.0"]